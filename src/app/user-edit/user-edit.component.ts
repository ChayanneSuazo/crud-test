import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../model/user';
import { UserService } from '../core/services/user.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import Swal from 'sweetalert2';
import { faPhone, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  @Input() user: User;
  users: User[];
  userIndex: number;
  userForm: User;
  public formGroup: FormGroup;
  faPhone = faPhone;
  faTrash = faTrash;

  constructor(private route: ActivatedRoute, private userService: UserService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.buildForm();
    this.getUser();
  }

  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.users = JSON.parse(localStorage.getItem('db'));
    this.user = this.users.find(x => x.id === id);
    this.userIndex = this.users.findIndex(x => x.id === id);
    this.initializeFields();
  }

  private buildForm() {
    this.formGroup = this.formBuilder.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      age: [null, [Validators.required]],
      phones: this.formBuilder.array([]),
      occupation: [null, [Validators.required]]
    });
  }

  createItem(phone?): FormGroup {
    if (phone) {
      return this.formBuilder.group({
        phone: [phone, [Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        phone: ['', [Validators.required]]
      });
    }
  }

  get phones(): FormArray {
    return this.formGroup.get('phones') as FormArray;
  }

  addItem(phone?): void {
    if (phone) {
      this.phones.push(this.createItem(phone));
    } else {
      this.phones.push(this.createItem());
    }
  }

  deleteItem(index): void {
    this.phones.removeAt(index);
  }

  initializeFields() {
    const userPhones = { ...this.user.phones };
    const keys = Object.keys(userPhones);

    this.formGroup.patchValue({
      name: this.user.name,
      email: this.user.email,
      age: this.user.age,
      occupation: this.user.occupation
    });
    keys.map(key => (
      this.addItem(userPhones[key].phone)
    ));
  }

  public getError(controlName: string): string {
    let error = '';
    const control = this.formGroup.get(controlName);
    if (control.touched && control.errors != null) {
      error = JSON.stringify(control.errors);
    }
    return error;
  }

  saveUser() {
    this.userForm = this.formGroup.value;
    this.userForm.id = this.user.id;
    this.userService.updateUser(this.userForm).subscribe();
    this.users[this.userIndex] = this.userForm;
    localStorage.setItem('db', JSON.stringify(this.users));
    Swal.fire({
      title: 'Usuario Editado',
      text: 'Tu usario ha sido editado correctamente',
      icon: 'success'
    });
    this.router.navigate(['/users']);
  }

  actionCancel(): void {
    Swal.fire({
      title: 'Deseas continuar ?',
      text: 'Los cambios que has realizado no seran guardados',
      icon: 'error',
      showConfirmButton: true,
      showCancelButton: true,
    }).then(willCancel => {
      if (willCancel.value) {
        this.router.navigate(['/users']);
      }
    });
  }
}
