import { User } from './../model/user';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { UserService } from '../core/services/user.service';
import Swal from 'sweetalert2';
import { faPhone, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Router } from "@angular/router";

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  public formGroup: FormGroup;
  users: any = [];
  user: User;
  faPhone = faPhone;
  faTrash = faTrash;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private route: Router) { }

  ngOnInit() {
    this.buildForm();
    this.getUsers();
  }

  private buildForm() {
    this.formGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      age: ['', [Validators.required]],
      phones: this.formBuilder.array([this.createItem()]),
      occupation: ['', [Validators.required]]
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      phone: ['', [Validators.required]]
    });
  }

  get phones(): FormArray {
    return this.formGroup.get('phones') as FormArray;
  }

  addItem(): void {
    this.phones.push(this.createItem());
  }

  deleteItem(index): void {
    this.phones.removeAt(index);
  }

  get genId(): number {
    let id: number;
    if (this.users.length != null) {
      id = Math.max(...this.users.map(x => x.id)) + 1;
      return id;
    } else {
      id = 1;
      return id;
    }
  }

  addUser(): void {
    this.user = this.formGroup.value;
    this.user.id = this.genId;
    this.userService.addUser(this.user).subscribe(data => {
      this.users.push(data);
      localStorage.setItem('db', JSON.stringify(this.users));
    });
    this.formGroup.reset();
    Swal.fire({
      title: 'Usuario Creado',
      text: 'Tu usario ha sido creado correctamente',
      icon: 'success'
    });
  }

  actionCancel(): void {
    Swal.fire({
      title: 'Deseas continuar ?',
      icon: 'error',
      showConfirmButton: true,
      showCancelButton: true,
    }).then(willCancel => {
      if (willCancel.value) {
        this.route.navigate(['/users']);
      }
    });
  }

  getUsers(): void {
    if (JSON.parse(localStorage.getItem('db')) != null) {
      this.users = JSON.parse(localStorage.getItem('db'));
    }

    // this.userService.getUsers().subscribe(data => {
    //   this.users = data;
    // });
  }

  public getError(controlName: string): string {
    let error = '';
    const control = this.formGroup.get(controlName);
    if (control.touched && control.errors != null) {
      error = JSON.stringify(control.errors);
    }
    return error;
  }
}
