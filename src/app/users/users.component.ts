import { Component, OnInit } from '@angular/core';
import { UserService } from '../core/services/user.service';
import { User } from '../model/user';
import { faTrash, faPencilAlt, faInfo } from '@fortawesome/free-solid-svg-icons';
import swal from 'sweetalert2';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  // Iconos
  faTrash = faTrash;
  faPencilAlt = faPencilAlt;
  faInfo = faInfo;
  // Iconos

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  // GET USERS
  getUsers(): void {

    if (JSON.parse(localStorage.getItem('db')) != null) {
      // Obteniendo informacion del LocalStorage
      this.users = JSON.parse(localStorage.getItem('db'));
    } else {
      // Simulando traer datos de la API
      this.userService.getUsers().subscribe(data => {
        console.log(data);
        this.users = data;
        localStorage.setItem('db', JSON.stringify(this.users));
      });
    }
  }

  // DELETE USER
  deleteUser(user: User): void {
    swal.fire({
      title: 'Estas seguro ?',
      text: 'Una vez eliminado, no podrá recuperar este usuario!',
      icon: 'warning',
      showConfirmButton: true,
      showCancelButton: true,
    }).then(willDelete => {
      if (willDelete.value) {
        this.users = this.users.filter(h => h !== user);
        this.userService.deleteUser(user).subscribe();
        localStorage.setItem('db', JSON.stringify(this.users));
        swal.fire({
          text: 'Usuario eliminado correctamente',
          icon: 'success'
        });
      }
    });

  }

}
