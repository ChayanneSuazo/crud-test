import { Component, OnInit } from '@angular/core';

// Importaciones
import { HttpClient } from '@angular/common/http';
import { User } from './model/user';
import { UserService } from './core/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'crudTest';

}
