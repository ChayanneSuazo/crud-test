import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from '../../model/user';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const users = [
      { id: 1, name: 'John Doe', email: 'jonh.doe@mail.com', phones: [{ "phone": '8098587441' }], age: 28, occupation: 'Policeman' },
      { id: 2, name: 'Louise White', email: 'louise.white@mail.com', phones: [{ "phone": '4705018113' }], age: 20, occupation: 'Nurse' },
      { id: 3, name: 'Phyllis Gardner', email: 'phyllis.gardner@mail.com', phones: [{ "phone": '7712762925' }], age: 22, occupation: 'Painter' },
      { id: 4, name: 'Tristan Price', email: 'tristan.price@mail.com', phones: [{ "phone": '1417166630' }, { "phone": '7712762925' }, { "phone": '1241513603' }], age: 38, occupation: 'Engineer' },
      { id: 5, name: 'Judy Thomas', email: 'judy.thomas@mail.com', phones: [{ "phone": '1503009276' }], age: 48, occupation: 'Teacher' },
      { id: 6, name: 'Gail Williamson', email: 'gail.williamson@mail.com', phones: [{ "phone": '1241513603' }], age: 18, occupation: 'Student' },
      { id: 7, name: 'Tammy Reid', email: 'tammy.reid@mail.com', phones: [{ "phone": '1969694473' }], age: 25, occupation: 'Journalist' },
      { id: 8, name: 'Leroy Baker', email: 'leroy.baker@mail.com', phones: [{ "phone": '7269013035' }], age: 23, occupation: 'Actor' },
      { id: 9, name: 'Julian Ternopolis', email: 'julian.ternopolis@mail.com', phones: [{ "phone": '8098587441' }], age: 55, occupation: 'Doctor' },
      { id: 10, name: 'Luciana Beltre', email: 'luciana.beltre@mail.com', phones: [{ "phone": '8098587441' }], age: 60, occupation: 'Singer' }
    ];


    return { users };
  }

  // Overrides the genId method to ensure that a user always has an id.
  // If the users array is empty,
  // the method below returns the initial number (11).
  // if the users array is not empty, the method below returns the highest
  // user id + 1.
  // genId(users: User[]): number {
  //   return users.length > 0 ? Math.max(...users.map(user => user.id)) + 1 : 11;
  // }
}
