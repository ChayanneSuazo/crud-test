export interface User {
  id?: number;
  name: string;
  email: string;
  phones: any;
  age: number;
  occupation: string;
}
